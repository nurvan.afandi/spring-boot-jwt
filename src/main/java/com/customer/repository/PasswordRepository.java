package com.customer.repository;

import com.customer.model.Password;
import com.customer.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;
@Repository
public interface PasswordRepository extends JpaRepository<Password, UUID> {

    Optional<Password> findByUsers(Users users);

}
