package com.customer.controller;

import com.customer.dto.request.AuthenticationCustomerRequest;
import com.customer.dto.request.AuthenticationRequest;
import com.customer.dto.request.RegisterRequest;
import com.customer.dto.response.AuthenticationResponse;
import com.customer.dto.response.BaseResponse;
import com.customer.service.AuthenticationService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthenticationService authenticationService;

    @PostMapping("register")
    public ResponseEntity<Object> register(
            @Valid @RequestBody RegisterRequest request
    ) throws MethodArgumentNotValidException {
        BaseResponse<AuthenticationResponse> response = authenticationService.register(request) ;

        return ResponseEntity
                .status(response.getStatus())
                .body(response);
    }

    @PostMapping("/login")
    public ResponseEntity<Object> authenticate(
            @Valid @RequestBody AuthenticationRequest request
    ) {
        BaseResponse<AuthenticationResponse> response = authenticationService.login(request);

        return ResponseEntity
                .status(response.getStatus())
                .body(response);
    }

    @PostMapping("/login/customer")
    public ResponseEntity<Object> loginCustomer(
            @Valid @RequestBody AuthenticationCustomerRequest request
    ) {
        BaseResponse<AuthenticationResponse> response = authenticationService.loginCustomer(request);

        return ResponseEntity
                .status(response.getStatus())
                .body(response);
    }
}
