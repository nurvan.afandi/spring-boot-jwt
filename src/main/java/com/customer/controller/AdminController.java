package com.customer.controller;

import com.customer.constant.Request;
import com.customer.dto.request.*;
import com.customer.dto.response.BaseResponse;
import com.customer.dto.response.CreateCustomerResponse;
import com.customer.dto.response.DetailCustomerResponse;
import com.customer.dto.response.ListCustomerResponse;
import com.customer.service.AdminService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/admin")
@RequiredArgsConstructor
public class AdminController {

    private final AdminService adminService;

    @PostMapping("/customer/create")
    public ResponseEntity<Object> create(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String bearer,
            @Valid @RequestBody CreateCustomerRequest request
    ) {
        request.setBearer(bearer);
        BaseResponse<CreateCustomerResponse> response = adminService.createCustomer(request);

        return ResponseEntity
                .status(response.getStatus())
                .body(response);
    }

    @GetMapping("/customer/detail/{id}")
    public ResponseEntity<Object> getDetailCustomer(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String bearer,
            @PathVariable("id") UUID id

    ) {
        DetailCustomerRequest request = new DetailCustomerRequest();
        request.setBearer(bearer);
        request.setId(id);

        BaseResponse<DetailCustomerResponse> response = adminService.detailCustomer(request);

        return ResponseEntity
                .status(response.getStatus())
                .body(response);
    }

    @GetMapping("/customer/list")
    public ResponseEntity<Object> getListCustomer(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String bearer,
            @RequestParam(defaultValue = Request.DEFAULT_PAGE) int page,
            @RequestParam(defaultValue = Request.DEFAULT_LIMIT) int limit
    ) {
        PaginationRequest requestPagination = new PaginationRequest(page, limit);
        ListCustomerRequest request = new ListCustomerRequest();
        request.setBearer(bearer);
        request.setRequestPagination(requestPagination);

        BaseResponse<List<ListCustomerResponse>> response = adminService.listDetailCustomer(request);

        return ResponseEntity
                .status(response.getStatus())
                .body(response);
    }

    @DeleteMapping("/customer/delete/{id}")
    public ResponseEntity<Object> deleteCustomer(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String bearer,
            @PathVariable("id") UUID id
    ) {
        DetailCustomerRequest request = new DetailCustomerRequest();
        request.setBearer(bearer);
        request.setId(id);
        BaseResponse<DetailCustomerResponse> response = adminService.deleteCustomer(request);

        return ResponseEntity
                .status(response.getStatus())
                .body(response);
    }

    @PutMapping("/customer/update")
    public ResponseEntity<Object> updateCustomer(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String bearer,
            @Valid @RequestBody UpdateCustomerRequest request
            ) {
        request.setBearer(bearer);
        BaseResponse<Object> response = adminService.updateCustomer(request);

        return ResponseEntity
                .status(response.getStatus())
                .body(response);
    }

}
