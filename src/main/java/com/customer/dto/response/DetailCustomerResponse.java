package com.customer.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DetailCustomerResponse {
    private String name;
    private String email;
    private String phone;

}
