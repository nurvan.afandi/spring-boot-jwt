package com.customer.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ListCustomerResponse {

    private String name;
    private String email;
    private String phone;

}
