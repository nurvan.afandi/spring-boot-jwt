package com.customer.dto.request;

import com.customer.dto.response.Metadata;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class PaginationRequest {

    @Setter
    private int page;
    @Setter
    @Getter
    private int limit;

    public int getPage() {
        return this.page - 1;
    }

    public Metadata toMetadata(int totalPage, Long totalData) {
        return new Metadata(getPage(), limit, totalPage, totalData);
    }

    public Pageable toPageable() {
        return PageRequest.of(getPage(), getLimit());
    }

    public final class PaginationUtil {
        private PaginationUtil(){}
        public static <T> Page<T> paginateList(final Pageable pageable, List<T> list) {
            int first = Math.min(new Long(pageable.getOffset()).intValue(), list.size());;
            int last = Math.min(first + pageable.getPageSize(), list.size());
            return new PageImpl<>(list.subList(first, last), pageable, list.size());
        }
    }

}
