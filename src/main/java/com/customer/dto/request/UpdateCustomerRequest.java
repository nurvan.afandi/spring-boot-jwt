package com.customer.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateCustomerRequest {

    @JsonIgnore
    private String bearer;

    @NonNull
    private String email;

    private String name;

    private String phone;

    private boolean active;

}
