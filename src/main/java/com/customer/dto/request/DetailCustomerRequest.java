package com.customer.dto.request;

import lombok.Data;

import java.util.UUID;

@Data
public class DetailCustomerRequest {
    private String bearer;
    private UUID id;

}
