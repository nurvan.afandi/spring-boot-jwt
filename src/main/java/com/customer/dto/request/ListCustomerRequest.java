package com.customer.dto.request;

import lombok.Data;

@Data
public class ListCustomerRequest {

    private String bearer;

    private PaginationRequest requestPagination;

}
