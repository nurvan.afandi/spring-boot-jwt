package com.customer.dto.request;

import com.customer.constant.Message;
import com.customer.constant.Role;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {
    @NotBlank(message = Message.NAME_NOT_BLANK)
    private String name;
    @NotBlank(message = Message.USERNAME_NOT_BLANK)
    private String username;
    @NotBlank(message = Message.PASSWORD_NOT_BLANK)
    private String password;

    private Role role;

}
