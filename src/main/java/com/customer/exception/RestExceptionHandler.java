package com.customer.exception;

import com.customer.constant.Message;
import com.customer.dto.response.BaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleValidationErrors(MethodArgumentNotValidException ex) {
        List<Map<String, String>> errors = new ArrayList<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String message = error.getDefaultMessage();

            Map<String, String> errorObject = Map.of(
                    "field", fieldName,
                    "message", message
            );

            errors.add(errorObject);
        });

        ex.getBindingResult().getAllErrors().stream()
                .map(objectError -> {
                    String fieldName = ((FieldError) objectError).getField();
                    String message = objectError.getDefaultMessage();

                    Map<String, String> errorObject = Map.of(
                            "field", fieldName,
                            "message", message
                    );

                    return errors.add(errorObject);
                });

        BaseResponse<Object> response = new BaseResponse<>(
                HttpStatus.UNPROCESSABLE_ENTITY.value(),
                HttpStatus.UNPROCESSABLE_ENTITY.name(),
                Message.ERROR_VALIDATION,
                errors
        );

        return ResponseEntity
                .status(response.getStatus())
                .body(response);
    }

}
