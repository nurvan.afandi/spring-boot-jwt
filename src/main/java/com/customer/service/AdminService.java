package com.customer.service;

import com.customer.dto.request.CreateCustomerRequest;
import com.customer.dto.request.DetailCustomerRequest;
import com.customer.dto.request.ListCustomerRequest;
import com.customer.dto.request.UpdateCustomerRequest;
import com.customer.dto.response.BaseResponse;
import com.customer.dto.response.CreateCustomerResponse;
import com.customer.dto.response.DetailCustomerResponse;
import com.customer.dto.response.ListCustomerResponse;

import java.util.List;

public interface AdminService {

    BaseResponse<CreateCustomerResponse> createCustomer(CreateCustomerRequest request);

    BaseResponse<DetailCustomerResponse> detailCustomer(DetailCustomerRequest request);

    BaseResponse<List<ListCustomerResponse>> listDetailCustomer(ListCustomerRequest request);

    BaseResponse<DetailCustomerResponse> deleteCustomer(DetailCustomerRequest request);

    BaseResponse<Object> updateCustomer(UpdateCustomerRequest request);

}
