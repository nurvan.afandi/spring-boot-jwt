package com.customer.service.impl;

import com.customer.constant.Message;
import com.customer.constant.Role;
import com.customer.dto.request.*;
import com.customer.dto.response.*;
import com.customer.model.Customers;
import com.customer.model.Users;
import com.customer.repository.CustomerRepository;
import com.customer.repository.UserRepository;
import com.customer.security.JwtUtil;
import com.customer.service.AdminService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {
    private final Logger logger = LoggerFactory.getLogger(AdminServiceImpl.class);
    private final UserRepository userRepository;
    private final CustomerRepository customerRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtil jwtUtil;

    @Override
    public BaseResponse<CreateCustomerResponse> createCustomer(CreateCustomerRequest request) {
        var customers = Customers.builder()
                .name(request.getName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .phone(request.getPhone())
                .role(Role.USER)
                .active(true)
                .build();

        customerRepository.save(customers);

        return new BaseResponse<>(
                HttpStatus.CREATED.value(),
                Message.REGISTER_CUSTOMER
        );
    }

    @Override
    public BaseResponse<DetailCustomerResponse> detailCustomer(DetailCustomerRequest request) {
        try {
            String username = jwtUtil.getUsernameFromBearer(request.getBearer());

            Users users = userRepository.findByUsername(username)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NOT_FOUND));

            Customers customers = customerRepository.findById(request.getId())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, Message.CUSTOMER_NOT_FOUND));

            if (users != null && users.getRole().equals(Role.USER)) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, Message.USER_FORBIDDEN);
            }

            return BaseResponse.<DetailCustomerResponse>builder()
                    .status(HttpStatus.OK.value())
                    .message(Message.USER_GET_CUSTOMER_DETAIL)
                    .data(
                            DetailCustomerResponse.builder()
                                    .name(customers.getName())
                                    .email(customers.getEmail())
                                    .phone(customers.getPhone())
                                    .build())
                    .build();
        } catch (ResponseStatusException e) {
            return BaseResponse.<DetailCustomerResponse>builder()
                    .status(e.getStatusCode().value())
                    .message(e.getMessage())
                    .additionalInfo(e.getReason())
                    .build();
        } catch (Exception e) {
            return BaseResponse.<DetailCustomerResponse>builder()
                    .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    .message(HttpStatus.INTERNAL_SERVER_ERROR.name())
                    .additionalInfo(e.getMessage())
                    .build();
        }
    }

    @Override
    public BaseResponse<List<ListCustomerResponse>> listDetailCustomer(ListCustomerRequest request) {
        try {
            String username = jwtUtil.getUsernameFromBearer(request.getBearer());

            Users users = userRepository.findByUsername(username)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NOT_FOUND));

            Pageable pageRequest = request.getRequestPagination().toPageable();

            List<Customers> customersList = customerRepository.findAll();

            Page<Customers> customersPage = PaginationRequest.PaginationUtil.paginateList(pageRequest, customersList);

            if (users != null && users.getRole().equals(Role.USER)) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, Message.USER_FORBIDDEN);
            }

            List<ListCustomerResponse> responses = customersPage.stream()
                    .map(customers -> {
                        return ListCustomerResponse.builder()
                                .name(customers.getName())
                                .email(customers.getEmail())
                                .phone(customers.getPhone())
                                .build();
                    })
                    .collect(Collectors.toList());

            Metadata metadata = request
                    .getRequestPagination()
                    .toMetadata(customersPage.getTotalPages(), customersPage.getTotalElements());

            return BaseResponse.<List<ListCustomerResponse>>builder()
                    .status(HttpStatus.OK.value())
                    .message(Message.USER_GET_LIST_CUSTOMER)
                    .data(responses)
                    .metadata(metadata)
                    .build();
        } catch (ResponseStatusException e) {
            return BaseResponse.<List<ListCustomerResponse>>builder()
                    .status(e.getStatusCode().value())
                    .message(e.getMessage())
                    .additionalInfo(e.getReason())
                    .build();
        } catch (Exception e) {
            return BaseResponse.<List<ListCustomerResponse>>builder()
                    .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    .message(HttpStatus.INTERNAL_SERVER_ERROR.name())
                    .additionalInfo(e.getMessage())
                    .build();
        }
    }

    @Override
    public BaseResponse<DetailCustomerResponse> deleteCustomer(DetailCustomerRequest request) {
        try {
            String username = jwtUtil.getUsernameFromBearer(request.getBearer());

            Users users = userRepository.findByUsername(username)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NOT_FOUND));

            if (users != null && users.getRole().equals(Role.USER)) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, Message.USER_FORBIDDEN);
            }

            customerRepository.deleteById(request.getId());

            return new BaseResponse<>(
                    HttpStatus.ACCEPTED.value(),
                    Message.USER_DELETE_CUSTOMER
            );
        } catch (ResponseStatusException e) {
            return BaseResponse.<DetailCustomerResponse>builder()
                    .status(e.getStatusCode().value())
                    .message(e.getMessage())
                    .additionalInfo(e.getReason())
                    .build();
        } catch (Exception e) {
            return BaseResponse.<DetailCustomerResponse>builder()
                    .status(HttpStatus.NO_CONTENT.value())
                    .message(HttpStatus.NO_CONTENT.name())
                    .additionalInfo(e.getMessage())
                    .build();
        }
    }

    @Override
    public BaseResponse<Object> updateCustomer(UpdateCustomerRequest request) {
        try {
            String username = jwtUtil.getUsernameFromBearer(request.getBearer());

            Users users = userRepository.findByUsername(username)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NOT_FOUND));

            if (users != null && users.getRole().equals(Role.USER)) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, Message.USER_FORBIDDEN);
            }

            Customers customers = customerRepository.findByEmail(request.getEmail())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, Message.CUSTOMER_NOT_FOUND));

            UUID id = customers.getId();

            boolean active;
            if (request.isActive()) {
                active = true;
            } else {
                active = false;
            }

            customers.setId(id);
            customers.setName(request.getName());
            customers.setEmail(request.getEmail());
            customers.setPhone(request.getPhone());
            customers.setActive(active);

            customerRepository.save(customers);

            return new BaseResponse<>(
                    HttpStatus.OK.value(),
                    Message.CUSTOMER_EDIT
            );
        } catch (ResponseStatusException e) {
            return BaseResponse.<Object>builder()
                    .status(e.getStatusCode().value())
                    .message(e.getMessage())
                    .additionalInfo(e.getReason())
                    .build();
        } catch (Exception e) {
            return BaseResponse.<Object>builder()
                    .status(HttpStatus.NO_CONTENT.value())
                    .message(HttpStatus.NO_CONTENT.name())
                    .additionalInfo(e.getMessage())
                    .build();
        }

    }

}
