package com.customer.service.impl;

import com.customer.constant.Message;
import com.customer.constant.Parameter;
import com.customer.constant.Role;
import com.customer.dto.request.AuthenticationCustomerRequest;
import com.customer.dto.request.AuthenticationRequest;
import com.customer.dto.request.RegisterRequest;
import com.customer.dto.response.AuthenticationResponse;
import com.customer.dto.response.BaseResponse;
import com.customer.model.Customers;
import com.customer.model.Password;
import com.customer.model.Users;
import com.customer.repository.CustomerRepository;
import com.customer.repository.PasswordRepository;
import com.customer.repository.UserRepository;
import com.customer.security.JwtUtil;
import com.customer.service.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.server.ResponseStatusException;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    Logger logger = LoggerFactory.getLogger(AuthenticationServiceImpl.class);
    private final UserRepository userRepository;
    private final CustomerRepository customerRepository;
    private final PasswordRepository passwordRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtil jwtUtil;
    private final AuthenticationManager authenticationManager;
    @Autowired
    public AuthenticationServiceImpl(UserRepository userRepository, CustomerRepository customerRepository, PasswordRepository passwordRepository, PasswordEncoder passwordEncoder, JwtUtil jwtUtil, AuthenticationManager authenticationManager) {
        this.userRepository = userRepository;
        this.customerRepository = customerRepository;
        this.passwordRepository = passwordRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtUtil = jwtUtil;
        this.authenticationManager = authenticationManager;
    }

    @Override
    public BaseResponse<AuthenticationResponse> register(RegisterRequest request) throws MethodArgumentNotValidException {
        try {
            var setPassword = passwordEncoder.encode(request.getPassword());

            Users user = Users.builder()
                    .name(request.getName())
                    .username(request.getUsername())
                    .password(setPassword)
                    .role(request.getRole() == null ? Role.USER : request.getRole())
                    .build();

            Password password = new Password(user, setPassword);

            userRepository.save(user);
            passwordRepository.save(password);

            return new BaseResponse<>(
                    HttpStatus.CREATED.value(),
                    Message.REGISTER_USER
            );
        } catch (ResponseStatusException e) {
            return new BaseResponse<>(
                    e.getStatusCode().value(),
                    e.getMessage(),
                    e.getReason()
            );
        } catch (Exception e) {
            return new BaseResponse<>(
                    HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    HttpStatus.INTERNAL_SERVER_ERROR.name(),
                    e.getMessage()
            );
        }
    }

    @Override
    public BaseResponse<AuthenticationResponse> login(AuthenticationRequest request) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            request.getUsername(),
                            request.getPassword()
                    )
            );

            Users users = userRepository.findByUsername(request.getUsername())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NOT_FOUND));

            Password password = passwordRepository.findByUsers(users)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, Message.USER_NOT_FOUND));

            boolean passwordAlreadyExpired = jwtUtil.passwordAlreadyExpired(password.getUpdatedAt());

            if (passwordAlreadyExpired) {
                return new BaseResponse<>(
                        HttpStatus.ACCEPTED.value(),
                        Message.USER_PASSWORD_EXPIRED,
                        new AuthenticationResponse(Parameter.BLANK));
            }

            var jwtToken = jwtUtil.generateToken(users);

            return new BaseResponse<>(
                    HttpStatus.OK.value(),
                    Message.LOGIN_SUCCESS,
                    new AuthenticationResponse(jwtToken));
        } catch (ResponseStatusException e) {
            return new BaseResponse<>(
                    e.getStatusCode().value(),
                    e.getMessage(),
                    e.getReason()
            );
        } catch (Exception e) {
            return new BaseResponse<>(
                    HttpStatus.UNAUTHORIZED.value(),
                    HttpStatus.UNAUTHORIZED.name(),
                    e.getMessage()
            );
        }
    }

    @Override
    public BaseResponse<AuthenticationResponse> loginCustomer(AuthenticationCustomerRequest request) {
        try {
             Customers customer = customerRepository.findByEmail(request.getEmail())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.FORBIDDEN, Message.CUSTOMER_NOT_FOUND));

            var jwtToken = jwtUtil.generateToken(customer);

            if (!customer.isActive()) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, Message.CUSTOMER_FORBIDDEN);
            }

            return new BaseResponse<>(
                    HttpStatus.OK.value(),
                    Message.LOGIN_SUCCESS,
                    new AuthenticationResponse(jwtToken));
        } catch (ResponseStatusException e) {
            return new BaseResponse<>(
                    e.getStatusCode().value(),
                    e.getMessage(),
                    e.getReason()
            );
        } catch (Exception e) {
            return new BaseResponse<>(
                    HttpStatus.UNAUTHORIZED.value(),
                    HttpStatus.UNAUTHORIZED.name(),
                    e.getMessage()
            );
        }
    }
}
