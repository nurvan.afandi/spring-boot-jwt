package com.customer.service;

import com.customer.dto.request.AuthenticationCustomerRequest;
import com.customer.dto.request.AuthenticationRequest;
import com.customer.dto.request.RegisterRequest;
import com.customer.dto.response.AuthenticationResponse;
import com.customer.dto.response.BaseResponse;
import org.springframework.web.bind.MethodArgumentNotValidException;

public interface AuthenticationService {

    BaseResponse<AuthenticationResponse> register(RegisterRequest request) throws MethodArgumentNotValidException;
    BaseResponse<AuthenticationResponse> login(AuthenticationRequest request);
    BaseResponse<AuthenticationResponse> loginCustomer(AuthenticationCustomerRequest request);

}
