package com.customer.constant;

public class Message {

    public static final String REGISTER_USER = "Berhasil daftar user";
    public static final String LOGIN_SUCCESS = "Login berhasil";
    public static final String REGISTER_CUSTOMER = "Berhasil daftar customer";
    public static final String USER_FORBIDDEN = "Akses dilarang untuk user ini";
    public static final String USER_GET_LIST_CUSTOMER = "Berhasil mengambil data customer";
    public static final String USER_GET_CUSTOMER_DETAIL = "Berhasil mengambil detail customer";
    public static final String USER_NOT_FOUND = "User tidak ditemukan";
    public static final String CUSTOMER_FORBIDDEN = "Akses dilarang";
    public static final String CUSTOMER_NOT_FOUND = "Customer tidak ditemukan";
    public static final String CUSTOMER_EDIT = "Customer berhasil diupdate";
    public static final String USER_DELETE_CUSTOMER = "Customer berhasil dihapus";
    public static final String USER_PASSWORD_EXPIRED = "Password expired";
    public final static String ERROR_VALIDATION = "Input error validation";
    public static final String NAME_NOT_BLANK = "Name must be not blank";
    public static final String USERNAME_NOT_BLANK = "Username must be not blank";
    public static final String PASSWORD_NOT_BLANK = "Password must be not blank";

}
