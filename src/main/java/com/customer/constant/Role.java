package com.customer.constant;

public enum Role {
    USER,
    ADMIN
}
