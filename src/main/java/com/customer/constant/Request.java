package com.customer.constant;

public class Request {

    public final static String DEFAULT_PAGE = "1";
    public final static String DEFAULT_LIMIT = "10";
    public final static String DEFAULT_SEARCH = "";
    public final static String DEFAULT_SEARCH_LIKE = "%%";

}
